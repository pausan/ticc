// -----------------------------------------------------------------------------
// Author: Pau Sanchez (contact@pausanchez.com)
// License: MIT
// -----------------------------------------------------------------------------
#ifndef __LEXER_H__
#define __LEXER_H__

#include <stdlib.h>

typedef enum {
  TOKEN_IDENTIFIER = 'i',
  TOKEN_TYPEDEF    = 't',
  TOKEN_STRUCT     = 's',

  TOKEN_NEWLINE    = '\n',
  TOKEN_LPAR       = '(',
  TOKEN_RPAR       = ')',

  TOKEN_SCOLON     = ';',
  TOKEN_LBRACE     = '{',
  TOKEN_RBRACE     = '}',

  // special tokens
  TOKEN_OTHER      = 'o',
  TOKEN_IGNORE     = 'I', // can be ignored

  // special token for end of file
  TOKEN_EOF        = '^',
} token_type_t;


// The idea is to parse tokens inplace, without allocations, so we should
// keep track of the token size
typedef struct {
  token_type_t type;
  const char  *text; // token pointer
  size_t       size; // token size
} token_t;


int token_next (token_t &next, const char *in, const char *end);

#endif /* __LEXER_H__ */