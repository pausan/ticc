// -----------------------------------------------------------------------------
// Author: Pau Sanchez (contact@pausanchez.com)
// License: MIT
// -----------------------------------------------------------------------------
#ifndef __FNV_H__
#define __FNV_H__

#include <stdint.h>

// -----------------------------------------------------------------------------
// fnvHash32v
//
// Simple, yet powerful algorithm to avoid collisions on english words based
// on FNV 1a. The last 'v' is because I've introduced a variation, since the
// result is initialized using first 4 words.
//
// Although as any algorithm, never collision free.
//
// Based on FNV/1a algorithm from:
//  http://isthe.com/chongo/tech/comp/fnv/
//
// See:
//   https://softwareengineering.stackexchange.com/questions/49550/which-hashing-algorithm-is-best-for-uniqueness-and-speed
// -----------------------------------------------------------------------------
inline uint32_t fnvHash32v(const uint8_t *data, size_t n) {
  const uint32_t PRIME = 16777619;
  uint32_t result = 0;

  // continue iterating through the rest of the string
  for(size_t i = 0; i < n; i++) {
    result ^= data[i];
    result *= PRIME;
  }

  return result;
}

#endif /* __FNV_H__ */
