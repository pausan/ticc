// -----------------------------------------------------------------------------
// Author: Pau Sanchez (contact@pausanchez.com)
// License: MIT
// -----------------------------------------------------------------------------
#include <ctype.h>

#include "lexer.h"

// -----------------------------------------------------------------------------
// token_next
//
// Gets the next token from the input. Uses a subset of tokens of C language
// because we are not interested in parsing C language, only interested in
// divide the file in statements/declarations and diff against other files.
//
// Returns 0 when there are no more tokens and 1 if you can keep asking for
// tokens
//
// NOTE: comments, preprocessor directives, etc... should have been stripped out
// -----------------------------------------------------------------------------
int token_next (token_t &next, const char *in, const char *end) {
  if (in >= end) {
    next.type = TOKEN_EOF;
    next.size = 0;
    next.text = in;
    return 0;
  }

  next.text = in;
  next.type = TOKEN_OTHER;
  next.size = 1;

  // identifier or keyword?
  if(isalpha(*in) || '_' == *in) {
    in++;

    next.type = TOKEN_IDENTIFIER;
    while (in < end && (isalnum(*in) || '_' == *in)) {
      next.size++;
      in++;
    }
  }
  else {
    switch (*in) {
      case '\n': next.type = TOKEN_NEWLINE; break;
      case '(': next.type = TOKEN_LPAR; break;
      case ')': next.type = TOKEN_RPAR; break;
      case ';': next.type = TOKEN_SCOLON; break;
      case '{': next.type = TOKEN_LBRACE; break;
      case '}': next.type = TOKEN_RBRACE; break;

      // ignore spaces, etc..
      case '\t':
      case '\r':
      case ' ' : next.type = TOKEN_IGNORE; break;

      // ignore till end of line (e.g # 1 "something.h" 3 5)
      case '#':
        in++;
        next.type = TOKEN_IGNORE;
        while (in < end && *in != '\n') {
          next.size++;
          in++;
        }
        break;

      default:
        // do nothing, already set to other
        break;
    }
  }

  return 1;
}
