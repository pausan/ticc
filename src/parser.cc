// -----------------------------------------------------------------------------
// Author: Pau Sanchez (contact@pausanchez.com)
// License: MIT
// -----------------------------------------------------------------------------
#include <stdio.h>
#include <string.h>

#include "debug.h"
#include "parser.h"
#include "lexer.h"
#include "fnv.h"

#define LEN_STATIC(string) (sizeof(string) - 1)
#define TOKEN_IS_KEYWORD_STATIC(token, keyword) \
  (  \
    (token.size == LEN_STATIC(keyword)) && \
    (strncmp(token.text, keyword, LEN_STATIC(keyword)) == 0) \
  )

// -----------------------------------------------------------------------------
// CodeBlockView::CodeBlockView
// -----------------------------------------------------------------------------
CodeBlockView::CodeBlockView () {
  clear();
}

// -----------------------------------------------------------------------------
// CodeBlockView::~CodeBlockView
// -----------------------------------------------------------------------------
CodeBlockView::~CodeBlockView () {
  clear();
}

// -----------------------------------------------------------------------------
// CodeBlockView::CodeBlockView
// -----------------------------------------------------------------------------
CodeBlockView::CodeBlockView (const CodeBlockView &org) {
  symbol_type = org.symbol_type;
  symbol_name = org.symbol_name;
  symbol_name_size = org.symbol_name_size;
  start = org.start;
  size = org.size;
  hash = org.hash;
  common = org.common;
}

// -----------------------------------------------------------------------------
// CodeBlockView::operator=
// -----------------------------------------------------------------------------
CodeBlockView &
CodeBlockView::operator= (const CodeBlockView &org) {
  symbol_type = org.symbol_type;
  symbol_name = org.symbol_name;
  symbol_name_size = org.symbol_name_size;
  start = org.start;
  size = org.size;
  hash = org.hash;
  common = org.common;

  return *this;
}

// -----------------------------------------------------------------------------
// CodeBlockView::operator==
// -----------------------------------------------------------------------------
bool
CodeBlockView::operator==(const CodeBlockView &other) const {
  return (
    (this->hash == other.hash)
    && (this->size == other.size)
    && (memcmp(this->start, other.start, this->size) == 0)
  );
}

// -----------------------------------------------------------------------------
// CodeBlockView::clear
// -----------------------------------------------------------------------------
void
CodeBlockView::clear (void) {
  symbol_type = SYMBOL_TYPE_UNKNOWN;
  symbol_name = NULL;
  symbol_name_size = 0;
  start = NULL;
  size = 0;
  hash = 0;
  common = false;
}

// -----------------------------------------------------------------------------
// CodeBlockView::parseNext
// -----------------------------------------------------------------------------
int
CodeBlockView::parseNext (const char *in, const char *end)
{
  this->start = in;
  this->size = 0;

  this->symbol_type = SYMBOL_TYPE_TYPE_DEFINITION;
  this->symbol_name = NULL;
  this->symbol_name_size = 0;

  token_t token;
  bool    keep_ignoring = true;
  int     nbraces = 0;
  int     npar = 0;
  bool    finished = false;
  bool    is_type_declaration = false;
  bool    is_func = false;
  token_type_t last_token_type = TOKEN_EOF;

  while (in < end && !finished && token_next(token, in, end)) {
    if (token.type == TOKEN_IGNORE || token.type == TOKEN_NEWLINE) {
      if (keep_ignoring)
        this->start = token.text + token.size;
      goto keep_going;
    }

    keep_ignoring = false;
    // debug (">>> %c --%.*s--\n", token.type, (int)token.size, token.text);

    // we are done at the end of a sentence if we are not defining or declaring anything
    if (token.type == TOKEN_SCOLON && (nbraces == 0)) {
      // non-functions finish with token and for functions we need
      // to know if we are talking about a function declaration or a func
      // definition C-style, with variables between the parameters
      // and the start of the body. Func definitions always have a ')' before
      // the ';' whereas the others don't.
      //
      // Example of declaration case:
      //   void something (type whatever) __attribute__ (xxx);
      //
      // Example of definition case c-style:
      //
      //   void something (a, b) __attribute__ (xxx)
      //      type a;
      //      other_type b;
      //   {
      //     ...
      //   }
      //
      // On the first case there is a ')' before the ';'
      //
      if (!is_func || (is_func && last_token_type == TOKEN_RPAR))
        finished = true;
    }

    else if (token.type == TOKEN_LPAR) {
      if (nbraces == 0 && npar == 0 && !is_type_declaration)
        is_func = true;
      npar++;
    }
    else if (token.type == TOKEN_RPAR) {
      npar--;
    }

    else if (token.type == TOKEN_LBRACE) {
      nbraces++;
    }
    else if (token.type == TOKEN_RBRACE) {
      nbraces--;

      // if we have a closing brace and we are not in a typedef/enum/struct/union
      // then when we find the '}' we are done... but of course there should not
      // be any brace nesting
      if (nbraces == 0 && !is_type_declaration)
        finished = true;

    }
    else if (token.type == TOKEN_IDENTIFIER) {
      is_type_declaration |= (nbraces == 0) && (npar == 0) && (
        TOKEN_IS_KEYWORD_STATIC(token, "typedef") ||
        TOKEN_IS_KEYWORD_STATIC(token, "enum") ||
        TOKEN_IS_KEYWORD_STATIC(token, "struct") ||
        TOKEN_IS_KEYWORD_STATIC(token, "union")
      );

      // debug(
      //   "IS TYPE DECL: %s // FUNC: %s // %.*s\n",
      //   is_type_declaration ? "YES" : "NO",
      //   is_func ? "YES" : "NO",
      //   (int)token.size,
      //   token.text
      // );
    }

    keep_going:
      in = token.text + token.size;
      last_token_type = token.type;
  }

  this->size = in - this->start;
  this->hash = fnvHash32v((const uint8_t *)this->start, this->size);

  return (in < end) || (this->size > 0);
}

// -----------------------------------------------------------------------------
// CodeBlockView::print
// -----------------------------------------------------------------------------
void
CodeBlockView::print (void) const
{
  printf (
    "[hash=%08x, size=%6d]\n%.*s\n",
    this->hash,
    (int)this->size,
    (int)this->size, this->start
  );
}
