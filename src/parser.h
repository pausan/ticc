// -----------------------------------------------------------------------------
// Author: Pau Sanchez (contact@pausanchez.com)
// License: MIT
// -----------------------------------------------------------------------------
#ifndef __PARSER_H__
#define __PARSER_H__

#include <stdint.h>
#include <stdlib.h>

typedef enum {
  SYMBOL_TYPE_UNKNOWN = 0,
  SYMBOL_TYPE_TYPE_DEFINITION,
  SYMBOL_TYPE_VARIABLE,
  SYMBOL_TYPE_FUNCTION,
} symbol_type_t;

// -----------------------------------------------------------------------------
// CodeBlockView contains pointers to either a definition or a declaration
// in a C file
// -----------------------------------------------------------------------------
class CodeBlockView {
  public:
    CodeBlockView();
    ~CodeBlockView();
    CodeBlockView(const CodeBlockView &org);
    CodeBlockView &operator=(const CodeBlockView &org);

    void clear(void);
    int parseNext (const char *in, const char *end);
    void print(void) const;

    bool operator==(const CodeBlockView &other) const;

  public:
    // const char **scopes; // in C, only one scope, not needed for now
    symbol_type_t  symbol_type;
    const char*    symbol_name;      // where it starts
    size_t         symbol_name_size; // lenght of the name

    // start/end for given symbol definition/declaration
    const char *   start;
    size_t         size;
    uint32_t       hash;

    // extra field for diffing
    bool           common;
};


#endif /* __PARSER_H__ */