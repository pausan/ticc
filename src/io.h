// -----------------------------------------------------------------------------
// Author: Pau Sanchez (contact@pausanchez.com)
// License: MIT
// -----------------------------------------------------------------------------
#ifndef __IO_H__
#define __IO_H__

typedef struct {
  int      fd;
  char    *data;
  size_t   size;
} mmapfile_t;

int mmapfile_open(mmapfile_t &mfile, const char *fname);
void mmapfile_close(mmapfile_t &mfile);

#endif /* __IO_H__ */