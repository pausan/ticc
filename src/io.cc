// -----------------------------------------------------------------------------
// Author: Pau Sanchez (contact@pausanchez.com)
// License: MIT
// -----------------------------------------------------------------------------
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>

#include "debug.h"
#include "io.h"

// -----------------------------------------------------------------------------
// mmapfile_open
//
// Open a memory mapped file by given name
//
// Returns 0 if succeeds and any other value if it doesn't
// -----------------------------------------------------------------------------
int mmapfile_open(mmapfile_t &mfile, const char *fname)
{
  int result = 0;
  struct stat sb;

  // init the structure
  mfile.fd = -1;
  mfile.data = NULL;
  mfile.size = 0;

  debug ("Processing %s...\n", fname);

  if ((mfile.fd = open(fname, O_RDONLY)) == -1) {
    printf ("ERROR: Cannot open file: %s\n", fname);
    mmapfile_close (mfile);
    return -1;
  }

  if (fstat(mfile.fd, &sb) == -1) {
    printf ("ERROR: cannot query file size for: %s\n", fname);
    mmapfile_close (mfile);
    return -2;
  }

  mfile.size = sb.st_size;

  mfile.data = (char*)mmap(
    NULL,                       // let the kernel allocate the space
    sb.st_size + 1,             // length: read the whole file (see: extra-char!)
    PROT_READ | PROT_WRITE,     // allow overwritting memory
    MAP_PRIVATE,                // don't flush to disk
    mfile.fd,                  // file to map
    0                           // no offset, read the whole file
  );
  if (mfile.data == MAP_FAILED) {
    printf ("ERROR: Could not mmap file: %s\n", fname);
    mmapfile_close (mfile);
    return -3;
  }

  return result;
}

// -----------------------------------------------------------------------------
// mmapfile_close
//
// closes a mmap file
// -----------------------------------------------------------------------------
void mmapfile_close(mmapfile_t &mfile) {
  if (mfile.fd != -1)
    close(mfile.fd);

  if (mfile.data != MAP_FAILED)
    munmap(mfile.data, mfile.size);

  mfile.fd = -1;
  mfile.data = NULL;
  mfile.size = 0;
}
