// -----------------------------------------------------------------------------
// Author: Pau Sanchez (contact@pausanchez.com)
// License: MIT
// -----------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unordered_map>

#include "debug.h"
#include "io.h"
#include "parser.h"

#define CLOCKS_PER_MILLIS   (CLOCKS_PER_SEC / 1000)

typedef enum {
  OPERATION_UNKNOWN,
  OPERATION_DIFF,
  OPERATION_PATCH,
  OPERATION_MERGE,
} operation_t;

// -----------------------------------------------------------------------------
// diff
// -----------------------------------------------------------------------------
int diff (const char *base_file, const char *edited_file) {
  mmapfile_t fbase;
  mmapfile_t fedited;

  if (mmapfile_open (fbase, base_file) != 0) {
    printf ("Cannot open %s\n", base_file);
    return -1;
  }

  if (mmapfile_open (fedited, edited_file) != 0) {
    printf ("Cannot open %s\n", edited_file);
    return -1;
  }

  typedef std::unordered_map<uint32_t, CodeBlockView> HashMapping;

  CodeBlockView code_unit;
  HashMapping base_lookup;
  HashMapping edited_lookup;

  const char *in = NULL;
  const char *in_end = NULL;


  in = fbase.data;
  in_end = fbase.data + fbase.size;
  while (code_unit.parseNext(in, in_end)) {
    base_lookup[code_unit.hash] = code_unit;
    in = code_unit.start + code_unit.size + 1;
  }

  in = fedited.data;
  in_end = fedited.data + fedited.size;
  while (code_unit.parseNext(in, in_end)) {
    edited_lookup[code_unit.hash] = code_unit;
    in = code_unit.start + code_unit.size + 1;
  }

  // now we have two maps with the hashes of the blocks
  // let's find out which blocks are in A and not in B and the other
  // way around
  for(
    auto base_it = base_lookup.begin();
    base_it != base_lookup.end();
    base_it ++
  )
  {
    auto edit_it = edited_lookup.find (base_it->first);
    if (edit_it != edited_lookup.end()) {
      // NOTE: collisions should be pretty rare with fnv, but in order to avoid
      //       them completely both code block strings need to be compared
      if (base_it->second == edit_it->second) {
        base_it->second.common = true;
        edit_it->second.common = true;
      }
    }
  }

  // at this point, in one iteration, all items in base/edit whose used
  // is false, means it is contained in base or edited but not in both

#ifdef DEBUG
  debug ("###################################\n");
  debug ("### COMMON BLOCKS\n");
  debug ("###################################\n\n");
  for(
    auto base_it = base_lookup.begin();
    base_it != base_lookup.end();
    base_it ++
  )
  {
    if(!base_it->second.common)
      continue;

    debug ("------------------------------------------------------\n");
    base_it->second.print();
  }

  debug ("###################################\n");
  debug ("### BLOCKS ONLY IN BASE\n");
  debug ("###################################\n\n");
  for(
    auto base_it = base_lookup.begin();
    base_it != base_lookup.end();
    base_it ++
  )
  {
    if(base_it->second.common)
      continue;

    debug ("------------------------------------------------------\n");
    base_it->second.print();
  }

  debug ("###################################\n");
  debug ("### BLOCKS ONLY IN EDITED\n");
  debug ("###################################\n\n");
  for(
    auto edit_it = edited_lookup.begin();
    edit_it != edited_lookup.end();
    edit_it ++
  )
  {
    if(edit_it->second.common)
      continue;

    debug ("------------------------------------------------------\n");
    edit_it->second.print();
  }
#endif

  mmapfile_close(fedited);
  mmapfile_close(fbase);

  return 0;
}


// -----------------------------------------------------------------------------
// main
// -----------------------------------------------------------------------------
int main (int argc, const char *argv[])
{
  if (argc != 4) {
    printf ("Use: %s --diff  base-file.c file-updated.c\n", argv[0]);
    printf ("     %s --patch base-file.c file-updated.c\n", argv[0]);
    printf ("     %s --merge base-file.o file-updated.o\n", argv[0]);
    return -1;
  }

  operation_t operation = OPERATION_UNKNOWN;
  if (strcmp (argv[1], "--diff") == 0) {
    operation = OPERATION_DIFF;
  }
  else if (strcmp (argv[1], "--patch") == 0) {
    operation = OPERATION_PATCH;
  }
  else if (strcmp (argv[1], "--merge") == 0) {
    operation = OPERATION_MERGE;
  }
  else {
    printf ("Invalid operation: %s\n", argv[1]);
    return -1;
  }

  const char *base_file = argv[2];
  const char *edited_file = argv[3];

  int result = -1;
  switch (operation) {
    case OPERATION_DIFF:
      debug ("Diffing %s %s\n", base_file, edited_file);
      result = diff (base_file, edited_file);
      break;

    case OPERATION_PATCH:
      debug ("Patching %s %s\n", base_file, edited_file);
      break;

    case OPERATION_MERGE:
      debug ("Merging %s %s\n", base_file, edited_file);
      break;

    default:
      printf ("Invalid command line operation\n");
  }

  return result;
}