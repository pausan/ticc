# Tiny Incremental Compiler Companion

This is just an experiment to enable incremental compilation of C programs
in a compiler agnostic way.

This is not even alpha software yet, this is just a fun project to play and
experiment with. Use or extend at your own risk.

This might help compilers do their job faster on incremental operations.

Uses preprocessed output from compilers, so we don't really need a full
C-compliant compliant parser with preprocessor, etc...

For the sake of simplicity C language has been chosen as the source language
because C++ contains more complex constructs in the language, like namespaces,
classes, templates, ... and this is just a proof of concept.

## Test

Just compile with -DDEBUG and run:

    $ make
    $ ./ticc --diff test/files/preprocessed01.c test/files/preprocessed01-edit.c

This will show the differences between both files.

## The hypothesis + approach

The hypothesis is that the compilers do a lot of repeated work when compiling
same functions over and over again upon a change in the file or a header file
that can hopefully be avoided.

One approach would be to implement some sort of caching in the compiler, but this
approach would add a lot of complexity on top of already complex software, and
another downside is that, if works, you would have to implement the same thing
again for each compiler to get the benefits.

The approach this project opted to, which is more compiler-agnostic, is to:
- First compilation
  - generate preprocessed C & save to cache (base)
  - compile the object file

- Next compilations
  - generate preprocessed C (edited)
  - compare with cached/base file
  - cleanup all bodies of functions that have not changed themselves
    or have not been impacted by a data type or global variable change
  - generate a new preprocessed C (cleanup) from previous step
  - invoke the compiler with the cleaned up file (should be faster, since the
    compiler has less things to do) into a new object file in a temp dir
  - patch the original object file by getting rid of the symbols that have
    changed, and append the new ones, with the new sections of code and/or data

It looks like a lot of stuff to do, to even gain something, but the compiler
spends the most time generating code for functions, not evaluating types nor
symbols, so, it might work.

If this tool ever works, it won't certainly replace ccache or similar tools,
but can serve as an extension to such tools, avoiding full recompilation on
certain situations (depending on the time it took to compile the file the first
time it might not be worth, and maybe other considerations).

In the case of C++ there can be bigger gains if lots of files are included with
templates that are still used in the file, but we only changed one of the methods
of the file and not the others. But still, this project focuses for now in C
files.

## Cases

Here is a list of cases that should force recompiling code.

### Global variable changes type

Should compile all functions using that variable.

For example, from:

```c
    uint32_t gVar;

    int method1(int a, int b) {
      return a + b;
    }

    int method2(int a, int b) {
      return a + b + gVar;
    }
```

to:

```c
    uint32_t gVar;

    int method1(int a, int b) {
      return a + b;
    }

    int method2(int a, int b) {
      return a + b + gVar;
    }
```

This should be recompiled:

```c
    uint32_t gVar;

    int method2(int a, int b) {
      return a + b + gVar;
    }
```

But method1 is still the same code, same syntax, same types, ... so it should
remain unaffected.

### Type changes its definition

Should compile all functions that take that type as a parameter or that use the
type in a local variable or that use a global variable of that type in the body.

For example, from:

```c
    typedef struct {
      char     *str;
      uint8_t  len;
    } my_struct_t;
```

to:

```c
    typedef struct {
      char     *str;
      uint32_t  len;
    } my_struct_t;
```

This should force any function returning my_struct_t, using it as parameter
or in the function body, to recompile.

### Function declaration or definition changes

If the signature of the function or its body changes, then of course we need
to recompile it.

### Removal of declarations, definitions or functions

Nothing needs to be really recompiled (since we generated a preprocessed file,
the compiler would have detected if any other was referencing that code).

We might get the list of symbols that have been removed in order to update
the object file and hide them for the linker (in case it has been moved
somewhere else).

### Everything else

If the function body is not affected by any of the things mentioned above,
then the definition should be converted to a declaration. The rest of
declarations and global varibles if they are used in the bodies of the
functions that will be recompiled, should stay.
