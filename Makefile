BUILD_DIR := build

CPP    := g++
CFLAGS := -g -O3 -pedantic -Wall -Wextra -pthread -DDEBUG
SRC_CC := $(wildcard src/*.cc)
OBJ_CC := $(foreach file,$(SRC_CC),$(BUILD_DIR)/$(file:.cc=.o))
DEP_CC := $(OBJ_CC:.o=.d)
OUTPUT := ticc

all: $(OUTPUT) check

$(BUILD_DIR):
	mkdir -p $@/src

$(BUILD_DIR)/%.o: %.cc
	$(CPP) $(CFLAGS) -c -o $@ $<

$(OUTPUT): $(BUILD_DIR) $(OBJ_CC)
	$(CPP) $(CFLAGS) -o $@ $(OBJ_CC)

clean:
	rm -rf $(BUILD_DIR) $(OUTPUT) $(UT_OUTPUT)

.PHONY: clean check

dbg-%:
	@echo "$* = $($*)"

-include  $(DEP_CC) $(UT_DEP)

# test:
# 	/usr/bin/time -v ./ticc --diff a-base.pp.c a-01.pp.c
# 	hyperfine -w 3 -m 300 "./ticc --diff a-base.pp.c a-01.pp.c"
