
typedef long unsigned int size_t;

typedef struct {
  long long __max_align_ll __attribute__((__aligned__(__alignof__(long long))));
  long double __max_align_ld __attribute__((__aligned__(__alignof__(long double))));
# 426 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h" 3 4
} max_align_t;


# 120 "/usr/include/stdlib.h" 3 4
# 220 "/usr/include/stdlib.h" 3 4
# 320 "/usr/include/stdlib.h" 3 4
extern
int atexit (
  void ( * __func ) (void))
  __attribute__ ((__nothrow__ , __leaf__))
  __attribute__ (
    (__nonnull__ (1))
);

# 420 "/usr/include/stdlib.h" 3 4
# 520 "/usr/include/stdlib.h" 3 4

extern void qsort (void *__base, size_t __nmemb, size_t __size,
     __compar_fn_t __compar) __attribute__ ((__nonnull__ (1, 4)));


extern unsigned long int something (void);


typedef char charf;
typedef int intf;
typedef uInt uIntf;
typedef uLong uLongf;

typedef struct
{
  unsigned long int __val[(1024 / (8 * sizeof (unsigned long int)))];
} __sigset_t;


struct __pthread_cond_s
{
  __extension__ union
  {
    __extension__ unsigned long long int __wseq;
    struct
    {
      unsigned int __low;
      unsigned int __high;
    } __wseq32;
  };
  __extension__ union
  {
    __extension__ unsigned long long int __g1_start;
    struct
    {
      unsigned int __low;
      unsigned int __high;
    } __g1_start32;
  };
  unsigned int __g_refs[2] ;
  unsigned int __g_size[2];
  unsigned int __g1_orig_size;
  unsigned int __wrefs;
  unsigned int __g_signals[2];
};


enum {
  _CS_PATH,
  _CS_LFS_CFLAGS = 1000,
};

typedef struct z_stream_s {
  Bytef *next_in;
  uInt avail_in;
} z_stream;

struct z_stream_s2 {
  Bytef *next_in;
  uInt avail_in;
} z_stream2_var;

typedef voidpf (*alloc_func) (voidpf opaque, uInt items, uInt size)
# 520 "/usr/include/stdlib.h" 3 4
;

typedef void (*free_func) (voidpf opaque, voidpf address)   ;

extern __inline __attribute__ ((__always_inline__)) __attribute__ ((__gnu_inline__)) __attribute__ ((__artificial__)) char *
__attribute__ ((__nothrow__ , __leaf__)) stpncpy (char *__dest, const char *__src, size_t __n)
{
  if (__builtin_object_size (__dest, 2 > 1) != (size_t) -1
      && (!__builtin_constant_p (__n) || __n > __builtin_object_size (__dest, 2 > 1)))
    return __stpncpy_chk (__dest, __src, __n, __builtin_object_size (__dest, 2 > 1));
  return __stpncpy_alias (__dest, __src, __n);
}


uLong onething(strm, sourceLen)
  z_streamp strm;
  uLong sourceLen;
{
  deflate_state *s;
  uLong complen, wraplen;
  whatever();
  return 0;
}

long unsigned int
another(type1 strm, unsinged long int (*callback)(int, char*))
  __attribute__ ((__nonnull__ (1, 4)))
{
  whatever2();
  return 0;
}